import { React, useEffect, useState } from "react";

const BaseURL = "https://ghibliapi.herokuapp.com/films";

export default function Home({ setter }) {
  const [films, setFilms] = useState(null);

  const mouseStyle = {
    cursor: "pointer",
  };

  useEffect(() => {
    getData();
    async function getData() {
      const response = await fetch(BaseURL);
      const data = await response.json();

      setFilms(data);
    }
  }, []);

  return (
    <div className="card shadow">
      {films && (
        <div className="card-body prose">
          <h2 className="card-title text-left">Movie List</h2>

          <ul className="text-left">
            {films.map((film, index) => (
              <li key={index}>
                <a
                  className="no-underline"
                  style={mouseStyle}
                  onClick={(e) => setter(film)}
                >
                  {film.title}
                </a>
              </li>
            ))}
          </ul>
        </div>
      )}
    </div>
  );
}
