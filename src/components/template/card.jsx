import React from "react";

const Card = () => {
  return (
    <div className="card col-span-2 row-span-3 shadow-lg xl:col-span-2 bg-base-100">
      <div className="card-body">
        <h2 className="my-4 text-4xl font-bold card-title">
          Top 10 UI Components
        </h2>
        <div className="mb-4 space-x-2 card-actions">
          <div className="badge badge-ghost">Colors</div>
          <div className="badge badge-ghost">UI Design</div>
          <div className="badge badge-ghost">Creativity</div>
        </div>
        <p>
          Rerum reiciendis beatae tenetur excepturi aut pariatur est eos. Sit
          sit necessitatibus veritatis sed molestiae voluptates incidunt iure
          sapiente.
        </p>
        <div className="justify-end space-x-2 card-actions">
          <button className="btn btn-primary">Login</button>
          <button className="btn">Register</button>
        </div>
      </div>
    </div>
  );
};

export default Card;
