import { React, useState } from "react";
import "./App.css";
import Home from "./components/pages/home";
import Header from "./components/template/header";
import Hero from "./components/template/hero";
import HomeDetail from "./components/pages/homeDetail";

function App() {
  const [film, setFilm] = useState(null);

  return (
    <div className="App">
      <Header />
      <Hero />
      <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-2 gap-6 mx-auto my-16">
        <Home setter={setFilm} />
        <HomeDetail film={film} />
      </div>
    </div>
  );
}

export default App;
